export const SERVER_URL = "http://localhost:8200";

export const USER_COOKIE_TOKEN_NAME = "places_user_token";

export const routes = {
    HOME: "/home",
    SIGNIN: "/signin",
    SIGNUP: "/signup",
    DISCOVER: "/discover",
    SAVED: "/saved",
    PROFILE: "/profile"
};
