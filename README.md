# Places client

Full information can be found in [Notion](https://www.notion.so/kfcgenius/Places-e4cf68b1a4504920be9f0560586eca30).  
Repository moved from [Github](https://github.com/asokolkov/places).

### Quick start

-   `docker build -t places-client-image .`
-   `docker run --name places-client -p 8100:8100 places-client-image`
